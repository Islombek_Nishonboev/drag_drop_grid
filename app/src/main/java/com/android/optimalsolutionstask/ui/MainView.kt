package com.android.optimalsolutionstask.ui

import com.android.optimalsolutionstask.data.db.entity.SampleEntity
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView : MvpView {

    fun getList(list: List<SampleEntity>)

}