package com.android.optimalsolutionstask.ui

import com.android.optimalsolutionstask.data.db.DbService
import com.android.optimalsolutionstask.data.db.entity.SampleEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import moxy.InjectViewState
import moxy.MvpPresenter
import org.koin.core.component.KoinComponent

@InjectViewState
class MainPresenter(var mDatabase: DbService) : MvpPresenter<MainView>(), KoinComponent {

    private var scope = CoroutineScope(
        Job() + Dispatchers.Main
    )

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        getList()
    }

    private fun getList() {
        scope.launch { viewState.getList(mDatabase.getList()) }
    }

    fun saveList(itemList: List<SampleEntity>) {
        scope.launch { mDatabase.saveList(itemList) }
    }

}