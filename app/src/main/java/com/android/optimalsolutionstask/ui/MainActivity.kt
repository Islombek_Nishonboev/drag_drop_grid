package com.android.optimalsolutionstask.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.view.menu.MenuAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import com.android.optimalsolutionstask.R
import com.android.optimalsolutionstask.data.db.entity.SampleEntity
import com.android.optimalsolutionstask.databinding.ActivityMainBinding
import com.android.optimalsolutionstask.ui.adapter.DragManageAdapter
import com.android.optimalsolutionstask.ui.adapter.MainRecyclerAdapter
import moxy.MvpAppCompatActivity
import moxy.MvpView
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get

class MainActivity : MvpAppCompatActivity(), MainView {

    @InjectPresenter
    lateinit var mPresenter: MainPresenter

    @ProvidePresenter
    fun provide(): MainPresenter = get()

    private lateinit var binding: ActivityMainBinding
    lateinit var mAdapter: MainRecyclerAdapter
    private val mList: MutableList<SampleEntity> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initRecycler()
        addItem()

    }

    private fun initRecycler() {
        mAdapter = MainRecyclerAdapter()
        binding.recyclerMain.layoutManager = GridLayoutManager(this, 3)
        binding.recyclerMain.adapter = mAdapter

        mAdapter.onServicePositionChange = { list -> mPresenter.saveList(list) }


        val helper = ItemTouchHelper(
            DragManageAdapter(
                mAdapter, this, ItemTouchHelper.UP.or(ItemTouchHelper.DOWN)
                    .or(ItemTouchHelper.LEFT)
                    .or(ItemTouchHelper.RIGHT),
                0
            )
        )
        helper.attachToRecyclerView(binding.recyclerMain)
    }


    // Function to add items to list when app installed for first time
    private fun addItem() {
        mList.add(SampleEntity("#f44336"))
        mList.add(SampleEntity("#d500f9"))
        mList.add(SampleEntity("#9e9d24"))
        mList.add(SampleEntity("#880e4f"))
        mList.add(SampleEntity("#4a148c"))
        mList.add(SampleEntity("#00b8d4"))
        mList.add(SampleEntity("#ffab00"))
        mList.add(SampleEntity("#03a9f4"))
        mList.add(SampleEntity("#76ff03"))
        mList.add(SampleEntity("#8e24aa"))
        mList.add(SampleEntity("#5e35b1"))
        mList.add(SampleEntity("#2196f3"))
        mList.add(SampleEntity("#039be5"))
        mList.add(SampleEntity("#388e3c"))
        mList.add(SampleEntity("#afb42b"))
        mList.add(SampleEntity("#9e9e9e"))
        mList.add(SampleEntity("#607d8b"))
        mList.add(SampleEntity("#536dfe"))
        mList.add(SampleEntity("#e040fb"))
        mList.add(SampleEntity("#e91e63"))
    }


    override fun getList(list: List<SampleEntity>) {
        if (list.isEmpty()) {
            mAdapter.updateItemList(mList)
        } else mAdapter.updateItemList(list)
    }
}