package com.android.optimalsolutionstask.ui.adapter

import android.app.Service
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.android.optimalsolutionstask.R
import com.android.optimalsolutionstask.data.db.entity.SampleEntity
import com.android.optimalsolutionstask.databinding.ItemMainBinding
import java.util.*

class MainRecyclerAdapter : RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>() {

    var mListItems: MutableList<SampleEntity> = mutableListOf()

    // function to listen changes
    var onServicePositionChange: (MutableList<SampleEntity>) -> Unit = { }

    fun updateItemList(itemList: List<SampleEntity>) {
        mListItems.clear()
        mListItems.addAll(itemList)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMainBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.initView(mListItems[position])
    }

    override fun getItemCount(): Int {
        return mListItems.size
    }

    inner class ViewHolder(private val binding: ItemMainBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun initView(item: SampleEntity) {
            binding.viewItem.setBackgroundColor(Color.parseColor(item.codeColor))
            binding.textItem.text = item.codeColor
            binding.textItem.setTextColor(Color.parseColor(item.codeColor))
        }
    }


    //Function to swap items
    fun swapItems(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(mListItems, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(mListItems, i, i - 1)
            }
        }

        notifyItemMoved(fromPosition, toPosition)
    }
}

/**
 * class adapter to handle drag and drop callbacks
 */
class DragManageAdapter(
    adapter: MainRecyclerAdapter,
    context: Context,
    dragDirs: Int,
    swipeDirs: Int
) :
    ItemTouchHelper.SimpleCallback(dragDirs, swipeDirs) {
    var mAdapter = adapter

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        mAdapter.swapItems(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

    }

    override fun onMoved(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        fromPos: Int,
        target: RecyclerView.ViewHolder,
        toPos: Int,
        x: Int,
        y: Int
    ) {
        // Saving positions after every change
        mAdapter.onServicePositionChange(mAdapter.mListItems)
    }
}