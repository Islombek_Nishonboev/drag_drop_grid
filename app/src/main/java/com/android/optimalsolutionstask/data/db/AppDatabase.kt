package com.android.optimalsolutionstask.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.optimalsolutionstask.data.db.dao.SampleDao
import com.android.optimalsolutionstask.data.db.entity.SampleEntity

@Database(entities = [SampleEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun sampleDao(): SampleDao

}