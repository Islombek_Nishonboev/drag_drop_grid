package com.android.optimalsolutionstask.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.optimalsolutionstask.data.db.entity.SampleEntity

@Dao
interface SampleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveList(items: List<SampleEntity>)

    @Query("SELECT* FROM sampleEntity")
    fun getList(): List<SampleEntity>

}