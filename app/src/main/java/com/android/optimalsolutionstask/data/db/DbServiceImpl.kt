package com.android.optimalsolutionstask.data.db

import com.android.optimalsolutionstask.data.db.entity.SampleEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DbServiceImpl(private val mDatabase: AppDatabase) : DbService {

    override suspend fun saveList(items: List<SampleEntity>) {
        return withContext(Dispatchers.IO) {
            return@withContext mDatabase.sampleDao().saveList(items)
        }
    }

    override suspend fun getList(): List<SampleEntity> {
        return withContext(Dispatchers.IO) {
            return@withContext mDatabase.sampleDao().getList()
        }
    }
}