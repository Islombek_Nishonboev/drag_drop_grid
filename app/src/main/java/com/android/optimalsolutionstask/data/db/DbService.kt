package com.android.optimalsolutionstask.data.db

import com.android.optimalsolutionstask.data.db.entity.SampleEntity

interface DbService {

    suspend fun saveList(items : List<SampleEntity>)
    suspend fun getList(): List<SampleEntity>

}