package com.android.optimalsolutionstask.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "sampleEntity")
class SampleEntity() {


    constructor( codeColor: String?) : this() {
        this.codeColor = codeColor
    }

    @NotNull
    @PrimaryKey
    @ColumnInfo(name = "code_color")
    var codeColor: String? = null

}