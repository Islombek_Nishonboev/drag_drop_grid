package com.android.optimalsolutionstask.di.module

import com.android.optimalsolutionstask.ui.MainPresenter
import org.koin.dsl.module

val presentationModule = module {

    factory { MainPresenter(get()) }

}