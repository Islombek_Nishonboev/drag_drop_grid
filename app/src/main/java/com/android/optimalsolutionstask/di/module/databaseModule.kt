package com.android.optimalsolutionstask.di.module

import android.content.Context
import androidx.room.Room
import com.android.optimalsolutionstask.data.db.AppDatabase
import com.android.optimalsolutionstask.data.db.DbService
import com.android.optimalsolutionstask.data.db.DbServiceImpl
import org.koin.dsl.module

val databaseModule = module {

    single { provideDatabaseService(get()) }
    factory { provideDatabase(get()) }

}

fun provideDatabaseService(database: AppDatabase): DbService {
    return DbServiceImpl(database)
}

fun provideDatabase(context: Context?): AppDatabase {
    return Room.databaseBuilder(context!!, AppDatabase::class.java, "optimalsolutions.db")
        .allowMainThreadQueries()
        .build()
}