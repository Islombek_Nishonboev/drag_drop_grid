package com.android.optimalsolutionstask.di

import com.android.optimalsolutionstask.di.module.databaseModule
import com.android.optimalsolutionstask.di.module.presentationModule

val appComponent = listOf(databaseModule, presentationModule)